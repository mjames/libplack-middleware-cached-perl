Source: libplack-middleware-cached-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Mason James <mtj@kohaaloha.com>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libclone-perl,
                     libplack-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libplack-middleware-cached-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libplack-middleware-cached-perl.git
Homepage: https://metacpan.org/release/Plack-Middleware-Cached
Rules-Requires-Root: no

Package: libplack-middleware-cached-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libplack-perl
Description: Glues a cache to your PSGI application
 Plack::Middleware::Cached can be used to glue a cache to a PSGI applications
 or middleware. A cache is an object that provides at least two methods to get
 and set data, based on a key. Existing cache modules on CPAN include CHI,
 Cache, and Cache::Cache. Although this module aims at caching PSGI
 applications, you can use it to cache any function that returns some response
 object based on a request environment.
 .
 Plack::Middleware::Cached is put in front of a PSGI application as
 middleware. Given a request in form of a PSGI environment E, it either
 returns the matching response R from its cache, or it passed the request to
 the wrapped application, and stores the application's response in the cache:
 .
 ________ _____
 .
 Request ===E===>| |---E--->| |
 .
 | Cached | | App |
 .
 Response <==R====|________|<--R----|_____|
 .
 This description was automagically extracted from the module by dh-make-perl.
